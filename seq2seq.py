from numpy import array
from numpy import argmax
from numpy import array_equal
from keras.utils import to_categorical
from keras.models import Model
from keras.layers import Input
from keras.layers import LSTM
from keras.layers import Dense
from keras.layers import Embedding
from keras.utils.vis_utils import plot_model
from keras.callbacks import ModelCheckpoint
from keras.layers import Flatten
from keras.layers import TimeDistributed
import gensim
import deepcut
import numpy as np
import csv
import difflib

# number of timesteps for encoder and decoder
encoded_length = 500
EMBEDDING_DIM = encoded_length
n_in = 10
n_out = 10
MAX_SEQUENCE_LENGTH = n_in

# preparing word vector
model_path = "wordembedding/corpus.th.model"
wv_model = gensim.models.Word2Vec.load(model_path)

max_word = len(wv_model.wv.vocab)
word_list = wv_model.wv.vocab

def similar_word(word):
    sim_word = difflib.get_close_matches(word, word_list)
    try:
        return sim_word[0]
    except:
        return "<NONE>"

def word2idx(word):
    index = 0
    try:
        index = wv_model.wv.vocab[word].index
    except:
        try:
            sim = similar_word(word)
            index = wv_model.wv.vocab[sim].index
        except:
            index = wv_model.wv.vocab["<NONE>"].index
    return index
def idx2word(idx):
  return wv_model.wv.index2word[idx]
def word_to_vector(inputword,size=encoded_length):
    try:
        vec = wv_model.wv[inputword]
        return vec
    except:
        return np.array([0]*size)
def vector_to_word(inputvector):
    try:
        similars = wv_model.wv.most_similar(positive=[inputvector, ])
        #word = wv_model.similar_by_vector(inputvector)
        return similars[0][0]
    except:
        return "<NONE>"

def wordcut(sentence):
    return deepcut.tokenize(sentence)

def padding_sequence(listsentence,maxseq):
    dataset = []
    for s in listsentence:
        n = maxseq - len(s)
        if n>0:
            dataset.append(s+(["<EOS>"]*n))
        elif n<0:
            dataset.append(s[0:maxseq])
        else:
            dataset.append(s)
    return dataset
def preparingword(listword):
    word =[]
    for w in listword:
        word.append(wordcut(w))
    return word
def word_embedding(listword):
    dataset = []
    for sentence in listword:
        tmp = []
        for w in sentence:
            tmp.append(word_to_vector(w))
        dataset.append(tmp)
    return np.array(dataset)
def word_index(listword):
    dataset = []
    for sentence in listword:
        tmp = []
        for w in sentence:
            tmp.append(word2idx(w))
        dataset.append(tmp)
    return np.array(dataset)

# load dataset
def load_data(datafile):
    dataX = []
    dataY = []
    data = open(datafile, "r").read().lower()
    for i in data.split("\n\n"):
        a = i.split("\n")
        question = a[0]
        answer = a[1]
        dataX.append(question)
        dataY.append(answer)
    return dataX,dataY
def load_csv_data(datafile):
    dataX = []
    dataY = []
    with open(datafile, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        next(reader)
        for row in reader:
            dataX.append(row[0])
            dataY.append(row[1])
    return dataX,dataY

def embedding_model():
    # define word embedding
    vocab_list = [(k, wv_model.wv[k]) for k, v in wv_model.wv.vocab.items()]
    embeddings_matrix = np.zeros((len(wv_model.wv.vocab.items()) + 1, wv_model.vector_size))
    for i in range(len(vocab_list)):
        word = vocab_list[i][0]
        embeddings_matrix[i + 1] = vocab_list[i][1]

    embedding_layer = Embedding(input_dim=len(embeddings_matrix),
                                output_dim=EMBEDDING_DIM,
                                weights=[embeddings_matrix],
                                trainable=False,name="Embedding")
    return embedding_layer,len(embeddings_matrix)


# returns train, inference_encoder and inference_decoder models
def define_models(n_input, n_output, n_units):

    encoder_inputs = Input(shape=(None, n_input))
    encoder = LSTM(n_units, return_state=True,name="Encoder_LSTM")
    encoder_outputs, state_h, state_c = encoder(encoder_inputs)
    encoder_states = [state_h, state_c]
    # define training decoder
    decoder_inputs = Input(shape=(None, n_output))
    decoder_lstm = LSTM(n_units, return_sequences=True, return_state=True,name="Decoder_LSTM")
    decoder_outputs, _, _ = decoder_lstm(decoder_inputs, initial_state=encoder_states)
    decoder_dense = Dense(n_output, activation='tanh')
    decoder_outputs = decoder_dense(decoder_outputs)
    model = Model([encoder_inputs, decoder_inputs], decoder_outputs)
    # define inference encoder
    encoder_model = Model(encoder_inputs, encoder_states)
    # define inference decoder
    decoder_state_input_h = Input(shape=(n_units,))
    decoder_state_input_c = Input(shape=(n_units,))
    decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
    decoder_outputs, state_h, state_c = decoder_lstm(decoder_inputs, initial_state=decoder_states_inputs)
    decoder_states = [state_h, state_c]
    decoder_outputs = decoder_dense(decoder_outputs)
    decoder_model = Model([decoder_inputs] + decoder_states_inputs, [decoder_outputs] + decoder_states)

    plot_model(encoder_model, to_file='encoder_model.png', show_shapes=True)
    plot_model(decoder_model, to_file='decoder_model.png', show_shapes=True)
    plot_model(model, to_file='model.png', show_shapes=True)

    # return all models
    return model, encoder_model, decoder_model

def ende_embedding_model(n_input, n_output, n_units):
    encoder_inputs = Input(shape=(None,), name="Encoder_input")

    encoder = LSTM(n_units,return_state=True, name='Encoder_lstm')
    Shared_Embedding,vocab_size = embedding_model()
    word_embedding_context = Shared_Embedding(encoder_inputs)
    encoder_outputs, state_h, state_c = encoder(word_embedding_context)
    encoder_states = [state_h, state_c]

    decoder_inputs = Input(shape=(None,), name="Decoder_input")
    decoder_lstm = LSTM(n_units, return_sequences=True, return_state=True, name="Decoder_lstm")
    word_embedding_answer = Shared_Embedding(decoder_inputs)
    decoder_outputs, _, _ = decoder_lstm(word_embedding_answer, initial_state=encoder_states)
    decoder_dense = TimeDistributed(Dense(vocab_size, activation='softmax', name="Dense_layer"))
    decoder_outputs = decoder_dense(decoder_outputs)
    model = Model([encoder_inputs, decoder_inputs], decoder_outputs)

    encoder_model = Model(encoder_inputs, encoder_states)
    decoder_state_input_h = Input(shape=(n_units,), name="H_state_input")
    decoder_state_input_c = Input(shape=(n_units,), name="C_state_input")
    decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
    decoder_outputs, state_h, state_c = decoder_lstm(word_embedding_answer, initial_state=decoder_states_inputs)
    decoder_states = [state_h, state_c]
    decoder_outputs = decoder_dense(decoder_outputs)
    decoder_model = Model([decoder_inputs] + decoder_states_inputs, [decoder_outputs] + decoder_states)
    return model, encoder_model, decoder_model

def train():
    data = "newnoi.th.csv"
    dataX = []
    dataY = []
    X1 = []
    X2 = []
    Y = []
    vecsize = encoded_length

    dataX,dataY = load_csv_data(data)
    dataX = preparingword(dataX)
    dataY = preparingword(dataY)

    for sentence in dataX:
        X1.append(sentence)
    for sentence in dataY:
        Y.append(sentence)
    for sentence in dataY:
        X2.append(["_"]+sentence[0:len(sentence)-1])

    X1 = padding_sequence(X1, n_in)
    X2 = padding_sequence(X2, n_out)
    Y = padding_sequence(Y, n_out)

    '''X1 = word_embedding(X1)
    X2 = word_embedding(X2)
    Y = word_embedding(Y)'''
    X1 = word_index(X1)
    X2 = word_index(X2)
    Y = word_index(Y)
    Y = to_categorical(Y, num_classes=max_word+1)

    train, infenc, infdec = ende_embedding_model(n_in,n_out,256)
    # define model
    #train, infenc, infdec = define_models(vecsize, vecsize, 1000)
    train.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['acc'])

    print(train.summary())
    # train model
    train.fit([X1, X2], Y, epochs=600)

    # saving model
    infenc.save_weights("model_enc_weight.h5")
    infenc.save("model_enc.h5")
    print("Saved model to disk")
    infdec.save_weights("model_dec_weight.h5")
    infenc.save("model_dec.h5")
    print("Saved model to disk")

    return 0

def test():
    # define model

    train, infenc, infdec = ende_embedding_model(n_in, n_out, 256)
    # load weights
    infenc.load_weights("model/newnoi/model_enc.h5")
    infdec.load_weights("model/newnoi/model_dec.h5")

    # start prediction
    while True:
        input_data = input()
        input_data = preparingword([input_data])
        input_data = padding_sequence(input_data, n_in)
        input_data = word_index(input_data)
        target = predict_sequence(infenc, infdec, input_data, n_out, encoded_length)
        int_target = onehot_to_int(target)
        ans = invert(int_target)
        print(ans)
    return 0

def onehot_to_int(inputvector):
    return np.argmax(inputvector, axis=1)

def invert(inputlist):
    sentence = []
    for w in inputlist:
        sentence.append(idx2word(w))
    return ("".join(sentence).replace("<EOS>"," "))

# generate target given source sequence
def predict_sequence(infenc, infdec, source, n_steps, cardinality):
    # encode
    state = infenc.predict(source)
    # start of sequence input
    target_seq = array(word_index("_"))
    # collect predictions
    output = list()
    for t in range(n_steps):
        # predict next char
        yhat, h, c = infdec.predict([target_seq] + state)
        # store prediction
        output.append(yhat[0,0,:])
        # update state
        state = [h, c]
        # update target sequence
        target_seq = np.array([[np.argmax(yhat[0,0,:])]])
    return array(output)

if __name__ == '__main__':
    train()