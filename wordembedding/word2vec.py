import gensim
import difflib

# preparing word vector
model_path = "corpus.th.model"
wv_model = gensim.models.Word2Vec.load(model_path)

word_list = wv_model.wv.vocab
sim_word = difflib.get_close_matches('หย', word_list)
print(sim_word)